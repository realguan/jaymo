Foo
  Boo
  Moo: b
  Too
    Zoo
234
  Tar
    Zar
123: 111
  Xar: c
    Xak
    Zak: d
      Tak: 345
  Rar
    000: a
-----
Foo
  Boo
  Too
    Zoo
234
  Tar
    Zar
123: 111
  Rar
    000: a
-----
Foo
234
  Tar
    Zar
123: 111
  Rar
    000: a
===== RESULT =====
"Foo"
"234"
  "Tar"
    "Zar"
"123": "111"
  "Rar"
    "000": 'a'
