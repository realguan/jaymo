String: foo
Int:    9
Bool:   true
---
String: nil
Int:    9
Bool:   true
---
String: foo
Int:    nil
Bool:   true
---
Invalid value for variable
===== RESULT =====
"Invalid value for variable"
