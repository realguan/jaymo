['a','b','c','d','e','f','g','h','i']
[]
['c']
['b','d','f']
['c','f','i',nil,nil]
-----
1i|2i
3i|4i
5i|6i
[]
[[5i,6i]]
[[3i,4i],[1i,2i]]
[[1i,2i],[5i,6i],nil,nil]
-----
'a' -> 1i
'b' -> 2i
'c' -> 3i
[]
[3i]
[2i,1i]
[1i,3i,nil]
===== RESULT =====
[1i,3i,nil]
