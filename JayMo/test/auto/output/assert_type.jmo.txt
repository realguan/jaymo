a
-----
b
-----
Error   : Assert failed
Message : Invalid type of current object!
Detail  : Type should be <Str>, but is <Char>
Call    : 'c'.assertType("Str")
Instance: Root
  @ assert_type.jmo :7    .assertType("Str")
  @ assert_type.jmo :7    'c'.tryUse(each.info)…
  @ assert_type.jmo :7    x<Object> = 'c'.tryUse(each.info)…
Invalid type of current object! | Type should be <Str>, but is <Char> | assert_type.jmo:7
-----
d
-----
e
-----
f
-----
Error   : Assert failed
Message : Invalid type of current object!
Detail  : Type should be <Number>, but is <Char>
Call    : 'g'.assertType(<Number>)
Instance: Root
  @ assert_type.jmo :15    .assertType(<Number>)
  @ assert_type.jmo :15    'g'.tryUse(each.info)…
  @ assert_type.jmo :15    x<Object> = 'g'.tryUse(each.info)…
Invalid type of current object! | Type should be <Number>, but is <Char> | assert_type.jmo:15
-----
h
-----
i
-----
===== RESULT =====
"i"
