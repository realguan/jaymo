Foo
Bar
Bak
-----
Foo
  Boo
  Moo
Bar
Bak
  Xar
    Xak
-----
Foo
  Boo
  Moo
  Too
    Zoo
Bar
  Tar
    Zar
Bak
  Xar
    Xak
    Zak
      Tak
-----
Zar
nil
Zar
Zar: Zzz
--
Bar
  Tar
    Zar: Zzz
-----
Foo
  Boo
  Moo
  Too
    Zoo
234
  Tar
    Zar: Zzz
123: 111
  Xar
    Xak
    Zak: 456
      Tak: 345
  Rar
    000: 567
-----
Foo
  Boo
  Moo
  Too
    Zoo
123: 111
  Xar
    Xak
    Zak: 456
      Tak: 345
  Rar
    000: 567
-----
Foo
  Moo
123: 111
  Xar
    Zak: 456
      Tak: 345
  Rar
    000: 567
-----
Foo
  Moo
123: 111
  Xar
    Zak: 456
      Tak: 345
  Rar
    000: 567
-----
Invalid value for root node | Can't set a value for root of the tree. | tree_1.jmo:49
===== RESULT =====
"Invalid value for root node | Can't set a value for root of the tree. | tree_1.jmo:49"
