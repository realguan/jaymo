FFFFFF - White
E8E8E8 - Very Light Gray
e4e8e8 - Very Light Bluish Gray
afb5c7 - Light Bluish Gray
9c9c9c - Light Gray
6b5a5a - Dark Gray
595D60 - Dark Bluish Gray
212121 - Black
6a0e15 - Dark Red
b30006 - Red
B22222 - Rust
FF6347 - Coral
FF4500 - Dark Salmon
F08080 - Salmon
FFDAB9 - Light Salmon
8c6b6b - Sand Red
89351d - Reddish Brown
532115 - Brown
330000 - Dark Brown
907450 - Dark Tan
dec69c - Tan
feccb0 - Light Nougat
FFAF7D - Nougat
E3A05B - Medium Nougat
E78B3E - Dark Nougat
774125 - Medium Brown
B3694E - Fabuland Brown
EF9121 - Fabuland Orange
E6881D - Earth Orange
b35408 - Dark Orange
FA5947 - Neon Orange
FF7E14 - Orange
FFA531 - Medium Orange
F7BA30 - Bright Light Orange
f7ad63 - Light Orange
E6C05D - Very Light Orange
DD982E - Dark Yellow
f7d117 - Yellow
F3E055 - Bright Light Yellow
ffe383 - Light Yellow
EbEE8F - Light Lime
DFEEA5 - Yellowish Green
BCEF66 - Neon Green
BDC618 - Medium Lime
A6CA55 - Lime
7C9051 - Olive Green
2E5543 - Dark Green
00642e - Green
10cb31 - Bright Green
62F58E - Medium Green
a5dbb5 - Light Green
76a290 - Sand Green
008a80 - Dark Turquoise
31B5CA - Light Turquoise
b5d3d6 - Aqua
CCFFFF - Light Aqua
143044 - Dark Blue
0057a6 - Blue
3399FF - Dark Azure
42C0FB - Medium Azure
61afff - Medium Blue
6BADD6 - Maersk Blue
9FC3E9 - Bright Light Blue
B4D2E3 - Light Blue
7DBFDD - Sky Blue
5a7184 - Sand Blue
506cef - Blue-Violet
2032B0 - Dark Blue-Violet
3448a4 - Violet
9391e4 - Medium Violet
C9CAE2 - Light Violet
5f2683 - Dark Purple
a5499c - Purple
DA70D6 - Light Purple
885E9E - Medium Lavender
E0AAD9 - Clikits Lavender
B18CBF - Lavender
B57DA5 - Sand Purple
B52952 - Magenta
C87080 - Dark Pink
F785B1 - Medium Dark Pink
FFBBFF - Bright Pink
FFC0CB - Pink
FFE1FF - Light Pink
===== RESULT =====
"FFE1FF - Light Pink"
