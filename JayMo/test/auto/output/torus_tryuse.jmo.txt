=== .tryUse   === Pure  === Stream   ===
1
-
Root
-----
Error   : Self defined error
Message : Foo
Detail  : Bar
Call    : Error.throw
Instance: Root
  @ torus_tryuse.jmo :22     Error.throw
  @ torus_tryuse.jmo :22     .proc(Error.throw)…
  @ torus_tryuse.jmo :22     1.tryUse("Foo")…
  @ torus_tryuse.jmo :101    this.test
  @ torus_tryuse.jmo :100    Try…
-
=== .tryUse   === Argument === Stream   ===
1
1
-
Root
Root
-----
Error   : Self defined error
Message : Foo
Detail  : Bar
Call    : Error.throw
Instance: Root
  @ torus_tryuse.jmo :34     Error.throw
  @ torus_tryuse.jmo :34     .proc(Error.throw)…
  @ torus_tryuse.jmo :34     1.tryUse("Foo")…
  @ torus_tryuse.jmo :34     Print
  @ torus_tryuse.jmo :101    this.test
  @ torus_tryuse.jmo :100    Try…
Foo
-
Foo
-----
Error   : Self defined error
Message : Foo
Detail  : Bar
Call    : Error.throw
Instance: Root
  @ torus_tryuse.jmo :40     Error.throw
  @ torus_tryuse.jmo :40     .proc(Error.throw)…
  @ torus_tryuse.jmo :40     1.tryUse("Foo")…
  @ torus_tryuse.jmo :40     Print
  @ torus_tryuse.jmo :101    this.test
  @ torus_tryuse.jmo :100    Try…
Foo
-
Foo
====================================================
=== .tryUse   === Pure  === Block    ===
Error   : Self defined error
Message : Foo
Detail  : Bar
Call    : Error.throw
Instance: Root
  @ torus_tryuse.jmo :50     Error.throw
  @ torus_tryuse.jmo :49     1.tryUse("Foo")…
  @ torus_tryuse.jmo :101    this.test
  @ torus_tryuse.jmo :100    Try…
-
-----
Error   : Self defined error
Message : Foo
Detail  : Bar
Call    : Error.throw
Instance: Root
  @ torus_tryuse.jmo :62     Error.throw
  @ torus_tryuse.jmo :61     1.tryUse(each.info)…
  @ torus_tryuse.jmo :101    this.test
  @ torus_tryuse.jmo :100    Try…
-
=== .tryUse   === Argument === Block    ===
Error   : Self defined error
Message : Foo
Detail  : Bar
Call    : Error.throw
Instance: Root
  @ torus_tryuse.jmo :74     Error.throw
  @ torus_tryuse.jmo :73     1.tryUse("Foo")…
  @ torus_tryuse.jmo :73     a = 1.tryUse("Foo")…
  @ torus_tryuse.jmo :101    this.test
  @ torus_tryuse.jmo :100    Try…
-> Foo
-
-> Foo
-----
Error   : Self defined error
Message : Foo
Detail  : Bar
Call    : Error.throw
Instance: Root
  @ torus_tryuse.jmo :88     Error.throw
  @ torus_tryuse.jmo :87     1.tryUse(each.info)…
  @ torus_tryuse.jmo :87     c = 1.tryUse(each.info)…
  @ torus_tryuse.jmo :101    this.test
  @ torus_tryuse.jmo :100    Try…
-> Foo | Bar | torus_tryuse.jmo:88
-
-> Foo | Bar | torus_tryuse.jmo:95
=== Finished ===
===== RESULT =====
Try
