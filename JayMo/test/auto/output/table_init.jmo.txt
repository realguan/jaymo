'a'|'a'|'a'|'a'|'a'|'a'|'a'|'a'
'a'|'a'|'a'|'a'|'a'|'a'|'a'|'a'
'a'|'a'|'a'|'a'|'a'|'a'|'a'|'a'
'a'|'a'|'a'|'a'|'a'|'a'|'a'|'a'
"Byte"|"Short"|"Int"|"Long"|"BigInt"|"Float"|"Dec"|"BigDec"
======|=======|=====|======|========|=======|=====|========
nil   |nil    |nil  |nil   |nil     |nil    |nil  |nil     
nil   |nil    |nil  |nil   |nil     |nil    |nil  |nil     
nil   |nil    |nil  |nil   |nil     |nil    |nil  |nil     
nil   |nil    |nil  |nil   |nil     |nil    |nil  |nil     
nil   |nil    |nil  |nil   |nil     |nil    |nil  |nil     
nil   |nil    |nil  |nil   |nil     |nil    |nil  |nil     
'a'|'a'|'a'
'a'|'a'|'a'
'b'|'b'|'b'
'b'|'b'|'b'
'c'|'c'|'c'
'c'|'c'|'c'
'd'|'d'|'d'
'd'|'d'|'d'
===== RESULT =====
'a'|'a'|'a'
'a'|'a'|'a'
'b'|'b'|'b'
'b'|'b'|'b'
'c'|'c'|'c'
'c'|'c'|'c'
'd'|'d'|'d'
'd'|'d'|'d'
