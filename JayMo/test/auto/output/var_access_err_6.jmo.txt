-- DIRECT --
1234
-- FROM ROOT OUT --
1234
-- FROM LOOP OUT --
1234
1234
-- FROM FUNCTION OUT --
-- FROM TYPE OUT --
4567
<Err>
Error   : Invalid source code
Message : Variable not initialized
Detail  : No value assigned to: bb
Call    : bb.print
Instance: Root
  @ var_access_err_6.jmo :6     bb.print
  @ var_access_err_6.jmo :52    r.incBB
  @ var_access_err_6.jmo :47    this.run
  @ var_access_err_6.jmo :62    TestType1
</Err>
===== RESULT =====
null
