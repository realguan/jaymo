7
<Err>
Error   : Invalid source code
Message : Variable without value.
Detail  : This Variable has no allocated value: b
Call    : b
Instance: Root
  @ let_right_err2.jmo :4    b
  @ let_right_err2.jmo :4    Print
</Err>
===== RESULT =====
null
