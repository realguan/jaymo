--- Test: a ---
Is number:    false
Is Letter:    true
Is case up:   false
Is case down: true
--- Test: B ---
Is number:    false
Is Letter:    true
Is case up:   true
Is case down: false
--- Test: - ---
Is number:    false
Is Letter:    false
Is case up:   false
Is case down: false
--- Test: . ---
Is number:    false
Is Letter:    false
Is case up:   false
Is case down: false
--- Test: 5 ---
Is number:    true
Is Letter:    false
Is case up:   false
Is case down: false
--- Test: ß ---
Is number:    false
Is Letter:    true
Is case up:   false
Is case down: true
--- Test: ä ---
Is number:    false
Is Letter:    true
Is case up:   false
Is case down: true
--- Test: + ---
Is number:    false
Is Letter:    false
Is case up:   false
Is case down: false
===== RESULT =====
nil
