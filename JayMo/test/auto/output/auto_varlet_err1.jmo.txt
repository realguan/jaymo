<Err>
Error   : Invalid source code
Message : Invalid argument!
Detail  : Argument 1 isn't a VarLet!
Call    : .amount(a,'d' > a)
Instance: Root
  @ auto_varlet_err1.jmo :2    .amount(a,'d' > a)
  @ auto_varlet_err1.jmo :2    "abcdef".amount(a,'d' > a)…
</Err>
===== RESULT =====
null
