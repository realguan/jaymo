----- true -----
true
true
1
1
1.0
----- false -----
false
false
0
0
0.0
----- -1 -----
false
-1
-1
(Int --> Char) -1
-1.0
----- 0 -----
false
0
0
0
0.0
----- 1 -----
true
1
1
1
1.0
----- 2 -----
true
2
2
2
2.0
----- a -----
true
a
(Char --> Int) 'a'
a
(Char --> Dec) 'a'
----- Z -----
true
Z
(Char --> Int) 'Z'
Z
(Char --> Dec) 'Z'
----- 0 -----
false
0
0
0
0.0
----- 1 -----
true
1
1
1
1.0
----- string -----
true
string
(Str --> Int) "string"
(Str --> Char) "string"
(Str --> Dec) "string"
-----  -----
false

(Str --> Int) ""
(Str --> Char) ""
(Str --> Dec) ""
----- true -----
true
true
(Str --> Int) "true"
(Str --> Char) "true"
(Str --> Dec) "true"
----- false -----
false
false
(Str --> Int) "false"
(Str --> Char) "false"
(Str --> Dec) "false"
----- abcdef -----
true
abcdef
(Str --> Int) "abcdef"
(Str --> Char) "abcdef"
(Str --> Dec) "abcdef"
----- 0.1 -----
true
0.1
0
(Dec --> Char) 0.1
0.1
----- 1.0 -----
true
1.0
1
1
1.0
----- -4.2 -----
false
-4.2
-4
(Dec --> Char) -4.2
-4.2
----- 4.2 -----
true
4.2
4
(Dec --> Char) 4.2
4.2
----- 0.0 -----
false
0.0
0
0
0.0
===== RESULT =====
nil
