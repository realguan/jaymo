Bar Root 9
Foo Root 9
F2B-9
Bar Root 123
Foo Root 123
F2B-123
Bar Root 234
Foo Root 234
Bar Root 345
Foo Root 345
<Err>
Error   : Invalid source code
Message : No Block allowed
Detail  : Function has no control functionality: Bar.foo…
Call    : Bar.foo…
Instance: Bar
  @ bugfix_2021-02-01_type_extends3b.jmo :18    Bar.foo…
  @ bugfix_2021-02-01_type_extends3b.jmo :18    Foo.foo…
  @ bugfix_2021-02-01_type_extends3b.jmo :18    Foo…
</Err>
===== RESULT =====
null
