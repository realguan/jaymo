Error["Foo","Bar","errors_1.jmo:3"]
<Error>
--------------------
2.5
5
<Int>
--------------------
oops1
Error   : Self defined error
Message : Foo
Detail  : Bar
Call    : Error.throw
Instance: Root
  @ errors_1.jmo :15    Error.throw
  @ errors_1.jmo :14    5.try…
  @ errors_1.jmo :18    it.show
Error["Foo","Bar","errors_1.jmo:15"]
<Error>
--------------------
===== RESULT =====
5i
