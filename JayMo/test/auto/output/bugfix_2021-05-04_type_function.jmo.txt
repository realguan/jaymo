<Err>
Error   : Invalid source code
Message : Misplaced new object
Detail  : Maybe missing comma before: 'Random'
Call    : 5.Random…
Instance: Root
  @ bugfix_2021-05-04_type_function.jmo :7    5.Random…
  @ bugfix_2021-05-04_type_function.jmo :7    this.foo(5.Random…)
</Err>
===== RESULT =====
null
