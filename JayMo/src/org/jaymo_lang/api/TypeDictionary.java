package org.jaymo_lang.api;

import org.jaymo_lang.object.A_Object;


/**
 * @author Michael Nitsche
 * @created 20.06.2022
 */
public interface TypeDictionary {

	String[] allTypes();

	Class<? extends A_Object> lookup(String typename);

}
