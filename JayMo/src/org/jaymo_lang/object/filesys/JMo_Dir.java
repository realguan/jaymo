/*******************************************************************************
 * Copyright (C): 2018-2022 Michael Nitsche
 *
 * This file is part of JayMo <https://www.jaymo-lang.org>.
 *
 * JayMo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * JayMo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with JayMo. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.jaymo_lang.object.filesys;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;

import org.jaymo_lang.error.ExternalError;
import org.jaymo_lang.model.Call;
import org.jaymo_lang.model.ObjectCallResult;
import org.jaymo_lang.object.A_Object;
import org.jaymo_lang.object.I_Object;
import org.jaymo_lang.object.atom.Bool;
import org.jaymo_lang.object.atom.Str;
import org.jaymo_lang.object.struct.JMo_List;
import org.jaymo_lang.runtime.CallRuntime;
import org.jaymo_lang.runtime.STYPE;
import org.jaymo_lang.util.Lib_Convert;

import de.mn77.base.data.struct.SimpleList;
import de.mn77.base.error.Err_FileSys;
import de.mn77.base.sys.Sys;
import de.mn77.base.sys.file.Lib_FileSys;


/**
 * @author Michael Nitsche
 */
public class JMo_Dir extends JMo_Path implements I_Object {

	public JMo_Dir() {
		super(new File(Sys.getCurrentDir()));
	}

	public JMo_Dir(final Call arg) {
		super(arg);
	}

	public JMo_Dir(final File f) {
		super(f);
	}

	@Override
	public String toString(final CallRuntime cr, final STYPE type) {
		final File f = this.getInternalFile();
		if(f == null)
			return "Dir";

		switch(type) {
			case REGULAR:
				return f.getAbsolutePath().toString();
			case NESTED:
			case IDENT:
				return "Dir";
			default:
				return "Dir(" + '"' + f.getAbsolutePath().toString() + '"' + ")";
		}
	}

	@Override
	protected ObjectCallResult call2(final CallRuntime cr, final String method) {
		final ObjectCallResult result = super.call2(cr, method);
		if(result != null)
			return result;
		else
			switch(method) {
				case "+":
				case "add":
					return A_Object.stdResult(this.mAdd(cr));

				// Content:
				case "list":
					return A_Object.stdResult(this.mList(cr, true, true));
				case "files":
					return A_Object.stdResult(this.mList(cr, true, false));
				case "dirs":
					return A_Object.stdResult(this.mList(cr, false, true));

				case "search":
				case "listRecursive":
					return A_Object.stdResult(this.mListRecursive(cr, true, true));
				case "searchFiles":
				case "filesRecursive":
					return A_Object.stdResult(this.mListRecursive(cr, true, false));
				case "searchDirs":
				case "dirsRecursive":
					return A_Object.stdResult(this.mListRecursive(cr, false, true));

				// Sub Dir / containing File:
				case "createDir":
					return A_Object.stdResult(this.mMakeDir(cr, false));
				case "makeDir":
					return A_Object.stdResult(this.mMakeDir(cr, true));
				case "deleteDir":
					return A_Object.stdResult(this.mDeleteSub(cr, true));
				case "deleteFile":
					return A_Object.stdResult(this.mDeleteSub(cr, false));


				// This Dir:
				case "create":
					return A_Object.stdResult(this.mCreate(cr, false));
				case "make":
					return A_Object.stdResult(this.mCreate(cr, true));
				case "rename":
					return A_Object.stdResult(this.mRename(cr));
				case "move":
					return A_Object.stdResult(this.mMove(cr));
				case "delete":
					return A_Object.stdResult(this.mDelete(cr));

				// Something else:
				// case "copy": result=copy(c); break;

				default:
					return null;
			}
	}

	private JMo_List iDirList(final CallRuntime cr, final SimpleList<I_Object> al, final File base, final boolean files, final boolean dirs, final String filter, final boolean recursive) {

		if(filter == null) {
			final File[] dirContent = base.listFiles();
			if(dirContent != null)
				for(final File f : dirContent) {
					this.iDirListAdd(al, files, dirs, f);
					if(recursive && f.isDirectory())
						this.iDirList(cr, al, f, files, dirs, filter, recursive);
				}
		}
		else {
			final DirectoryStream<java.nio.file.Path> content = this.searchFiles(cr, base, filter);
			for(final java.nio.file.Path d : content)
				this.iDirListAdd(al, files, dirs, d.toFile());

			if(recursive) {
				final File[] dirContent = base.listFiles();
				if(dirContent != null)
					for(final File f : dirContent)
						if(f.isDirectory())
							this.iDirList(cr, al, f, files, dirs, filter, recursive);
			}
		}
		return new JMo_List(al);
	}

	private void iDirListAdd(final SimpleList<I_Object> al, final boolean files, final boolean dirs, final File f) {
		final boolean isf = f.isFile();
		if(isf && files)
			al.add(new JMo_File(f));
		if(!isf && dirs)
			al.add(new JMo_Dir(f));
	}

	/**
	 * °+ ^ add
	 * °add(Str s)Dir # Create a new Dir object, where 's' is added to the path
	 */
	private JMo_Dir mAdd(final CallRuntime cr) {
		final I_Object arg = cr.args(this, Str.class)[0];
		final String argPath = Lib_Convert.getStringValue(cr, arg);
		final String currentAbsolutePath = this.getInternalFile().getAbsolutePath();
		final File f = new File(currentAbsolutePath + argPath);
		return new JMo_Dir(f);
	}

	/**
	 * °create()Same # Create this directory, throw error if it already exist.
	 * °make()Same # Create this directory, if it doesn't already exists.
	 */
	private JMo_Dir mCreate(final CallRuntime cr, final boolean lazy) {
		cr.argsNone();
		final String path = this.getInternalFile().getAbsolutePath();

		if(!lazy) {
			final File f = new File(path);
			if(f.exists())
				throw new ExternalError(cr, "Can't create Directory", "Directory already exists: " + path);
		}

		try {
			Lib_FileSys.createPathStructure(path);
		}
		catch(final Err_FileSys e) {
			throw new ExternalError(cr, "Can't create Directory", path);
		}
		return this;
	}

	/**
	 * °delete()Bool # Delete this directory if empty. Return 'true' if directory is deleted.
	 * //delete(Bool recursive)Bool # Delete this directory. Return 'true' if directory is deleted. !!!KEEP CARE!!!
	 */
	private Bool mDelete(final CallRuntime cr) {
		cr.argsNone();

		// ATTENTION
//		throw new ExtError(cr, "File-Delete-Forbidden", pFile().getCanonicalPath());

		final File f = this.getInternalFile();

		if(f.exists()) {
			final boolean deleted = f.delete(); // Deletes only empty dirs
//			if(!deleted)
//				throw new ExtError(cr, "File-Delete-Error", this.getFile().getAbsolutePath());
			return Bool.getObject(deleted);
		}
		return Bool.TRUE;
	}

	/**
	 * °deleteDir(Str name)Bool # Delete a child directory, if it is empty. Returns true if successful.
	 * °deleteFile(Str name)Bool # Delete a file in this directory. Returns true if successful.
	 */
	private Bool mDeleteSub(final CallRuntime cr, final boolean dir) {
		final Str arg = (Str)cr.args(this, Str.class)[0];
		final String dirName = arg.getValue();
		final String sep = Sys.getSeperatorDir();

		// No Path allowed
		if(dirName.contains(sep))
			throw new ExternalError(cr, "Invalid directory name", "Name contains directory separator: " + sep);

		final File f = new File(this.getInternalFile().getAbsolutePath() + sep + dirName);
		if(dir && f.isFile())
			throw new ExternalError(cr, "Delete error", "The given name is not a directory: " + f.getAbsolutePath());
		if(!dir && f.isDirectory())
			throw new ExternalError(cr, "Delete error", "The given name is not a file: " + f.getAbsolutePath());

		if(f.exists()) {
			final boolean deleted = f.delete(); // Only empty
			return Bool.getObject(deleted);
		}
		return Bool.TRUE;
	}

	/**
	 * °list()List # Generate a List with all files and directorys
	 * °list(Str filter)List # Generate a List with filtered files and directorys
	 * °files()List # Generate a List with all files in this directory
	 * °files(Str filter)List # Generate a List with filtered files and in this directory
	 * °dirs()List # Generate a List with all directorys in this directory
	 * °dirs(Str filter)List # Generate a List with filtered directorys and in this directory
	 */
	private JMo_List mList(final CallRuntime cr, final boolean files, final boolean dirs) {
		final I_Object[] args = cr.argsVar(this, 0, 1);
		final String filter = args.length == 0 ? null : ((Str)cr.argType(args[0], Str.class)).getValue();
		final SimpleList<I_Object> al = new SimpleList<>();
		return this.iDirList(cr, al, this.getInternalFile(), files, dirs, filter, false);
	}

	/**
	 * °listRecursive ^ search
	 * °filesRecursive ^ searchFiles
	 * °dirsRecursive ^ searchDirs
	 * °search()List # Generate a recursive List with all files and directorys
	 * °search(Str filter)List # Generate a recursive List with filtered files and directorys
	 * °searchFiles()List # Generate a recursive List with all files in this directory
	 * °searchFiles(Str filter)List # Generate a recursive List with filtered files and in this directory
	 * °searchDirs()List # Generate a recursive List with all directorys in this directory
	 * °searchDirs(Str filter)List # Generate a recursive List with filtered directorys and in this directory
	 */
	private JMo_List mListRecursive(final CallRuntime cr, final boolean files, final boolean dirs) {
		final I_Object[] args = cr.argsVar(this, 0, 1);
		final String filter = args.length == 0 ? null : ((Str)cr.argType(args[0], Str.class)).getValue();
		final SimpleList<I_Object> al = new SimpleList<>();
		return this.iDirList(cr, al, this.getInternalFile(), files, dirs, filter, true);
	}

	/**
	 * °createDir()Dir # Create a new child-directory. Throw error if it already exists.
	 * °makeDir()Dir # Create a new child-directory, if it doesn't already exists.
	 */
	private JMo_Dir mMakeDir(final CallRuntime cr, final boolean lazy) {
		final Str arg = (Str)cr.args(this, Str.class)[0];
		final String newDirName = arg.getValue();

		// No Path allowed
		if(newDirName.contains(Sys.getSeperatorDir())) // Not allowed to creating "/foo/bar/bak" at once
			throw new ExternalError(cr, "Can't create Directory", "No target-path allowed");

		final String newDir = this.getInternalFile() + Sys.getSeperatorDir() + newDirName;

		if(!lazy) {
			final File f = new File(newDir);
			if(f.exists())
				throw new ExternalError(cr, "Can't create Directory", "Directory already exists: " + newDir);
		}

		try {
			Lib_FileSys.createPathStructure(newDir);
		}
		catch(final Err_FileSys e) {
			throw new ExternalError(cr, "Can't create Directory", newDirName);
		}
		return new JMo_Dir(new File(newDir));
	}

	/**
	 * °move(Str newNameOrPath)FileSys # Move the file or directory
	 */
	private JMo_Path mMove(final CallRuntime cr) {
		final Str arg = (Str)cr.args(this, Str.class)[0];
		final String newPath = Lib_Convert.getStringValue(cr, arg);
		final File target = new File(newPath);

		try {
			Files.move(this.getInternalFile().toPath(), target.toPath());
			this.changeFile(target);
			return this;
		}
		catch(final IOException e) {
			throw new ExternalError(cr, "Directory-Move-Error", this.getInternalFile().getAbsolutePath() + " --> " + target.getAbsolutePath());
		}
	}

	/**
	 * °rename(Str newName)Dir # Rename the directory
	 */
	private I_Object mRename(final CallRuntime cr) {
		final Str arg = (Str)cr.args(this, Str.class)[0];
		String newName = Lib_Convert.getStringValue(cr, arg);

		// No Path allowed, only name!
		if(newName.contains(Sys.getSeperatorDir()))
			throw new ExternalError(cr, "Directory-Rename failed", "No target-path allowed");

		newName = this.getInternalFile().getParent() + Sys.getSeperatorDir() + newName;
		final File newFile = new File(newName);
		final boolean done = this.getInternalFile().renameTo(newFile);
		if(!done)
			throw new ExternalError(cr, "Directory-Rename failed", this.getInternalFile().getAbsolutePath() + " --> " + newFile.getAbsolutePath());
		this.changeFile(newFile);
		return this;
	}

	private DirectoryStream<java.nio.file.Path> searchFiles(final CallRuntime cr, final File base, final String filter) {

		try {
			return Lib_FileSys.searchFiles(base, filter);
		}
		catch(final IOException e) {
			throw new ExternalError(cr, "In/Out-Error", e.getMessage());
		}
	}

}
