=== 0.20.0 / 22.07.2022
* New:    Number.style: Thousands dot added with [*_]
* New:    Number.style: Optional dot added [:;]
* New:    Number.style: Decimals now are handled with [,.]
* New:    New function: jaymo.allTypes
* New:    New function: Object.tryLazy
* New:    Accept variable types with question mark: "Int?"
* New:    ServiceLoader integrated for mapping typename to java-class
* New:    New/Reworked: File.exec, File.open, File.cmd
* New:    List.as - Functions
* New:    StackTrace finally integrated
* New:    Rework of format string, new specials for Str: e,E,r,h,H,d
* New:    Functions for List: .hasNext, .detach, .detachFirst, .detachLast
* Change: File.getDir renamed to: .directory, .dir
* Change: Nil is now Immutable but not Atomic
* Change: Error messages improved
* Change: ParseError and CodeError combined
* Change: Speed improvement at parsing objects.
* Change: ExecError renamed to RuntimeError
* Change: Conversion from JayMo-List to Java-Arrays enhanced
* Change: Alias "Regex" removed. Now only "RegEx" is valid.
* Change: ExtError renamed to ExternalError
* Change: Alias removed: Chars.pos
* Change: Conversion of Char to Bool changed: only '0' and '\0' are false
* Change: Structure of Format-Strings changed to: "field:format"
* Change: Format-String: Str-Special 'e' changed to 'E'
* Change: Functions renamed: .area and .cutTo --> .part
* Change: A lot of functions renamed. "get" removed where its not necessary
* Change: Rework and fixes for TableBlock. Now comments are allowed.
* Bugfix: Number style with escape char
* Bugfix: ".types" for magic variables
* Bugfix: Nil.add
* Bugfix: List.min .max .avg
* Bugfix: Conversion of List to String
* Bugfix: Text-Block after String
* Bugfix: Ignore Jars within a Jar at class search
* Bugfix: Check if type exists
* Bugfix: Override of function '.compare' is not allowed
* Bugfix: Object.pipeToCmd: Argument is now quoted
* Bugfix: Error message for empty TableBlock
* Bugfix: Handling of StackOverflowError improved

=== 0.19.1 / 13.04.2022
* Change: Functionality of Str.split(IntNumber) moved to Str.group
* Change: Clearity for methods of Swing_Font
* Bugfix: Bugfix for BrickLinkAPI.createInventory
* Bugfix: Different small bugfixes

=== 0.19.0 / 04.04.2022
* New:    ImportManager now cat handle imports also within jars
* New:    New magic constants for AXIS
* New:    New escape char: \0
* New:    New ">strict" as alias for ">medium"
* New:    List.toUniqueMap
* New:    Map.count, Map.inc, Map.dec
* New:    Allow the execution of block only for exceptional cases.
* New:    Number.exp, Number.log10, Number.ln
* New:    Table.init
* New:    New classic Type: Sleep
* New:    Run external script with arguments: jaymo.run( "script.jmo", [] )
* New:    For event handlers now a stream is allowed
* New:    Complex default value for type parameters
* New:    Complex default value for function arguments
* New:    Self defined magic constants
* New:    Magic variable "super"
* New:    Object.inherits as alternative to .isType
* New:    List.readOnly & List.freeze
* New:    MemLet as base type for VarLet and ConstLet
* New:    Backslash functions and "\\" as alternative for "it"
* New:    Constant min and max values for all atomic types.
* New:    Number.exp
* New:    Final integration of BigInt and BigDec
* New:    jaymo.run  ( This executes  an external script file )
* New:    Allow override of .toIdent and .toDescribe
* New:    Ident, Object.ident
* New:    New magic variable and hot string execution: "jaymo"
* New:    Parserswitch with '=' is allowed again: '>prefix = "Foo"'
* New:    New parser switch: >noJava
* Change: Full access from events to variables is no allowed!
* Change: Imports are now relative from current file
* Change: Error handling improved for imports
* Change: Show also filename and path of jaymo-scripts in jar files
* Change: Rework of toString versions.
* Change: Millisec.: Time.toStr is without, Ident/Describe/toStrFull are with
* Change: Safemode renamed to "sandbox"
* Change: No Java-StackTrace output by default.
* Change: Table.getMap removed. Table.getRowMap should be used!
* Change: Clarity for Auto-Property: Without args is "get", with args is "set"
* Change: A lot or work at quote/unquote and escape/unescape
* Change: Format-Strings improved: "^v" removed, "cCe" added
* Change: Now App can return "null"
* Change: Modifier for definition of type/func with control ability changed: !!
* Change: Checks for function result == nil, moved to >high
* Change: Clearity: \\ is allowed like => without VarLet/ConstLet
* Change: Alias removed: Table.col --> Table.column
* Change: Rewrite of try-functions because of torus ability
* Change: Removed: Object.tryCatch, Try.blockCatch
* Change: Rework and clearity for: Object.tryUse
* Change: Clearity: ".has" and ".knows" renamed to ".contains"
* Change: Table.setTitles now with VarArgs
* Change: Show Java errors and trace for debugging
* Change: Complete rework of Atomic-Conversion. Now using Enums.
* Change: Atomic calculations reworked and improved
* Change: Str: '.split' and '.div' now have different functionalitys
* Change: Error messages improved
* Change: Removed: Copy current state of vars for event handlers: @event(var)
* Change: Enum improved
* Change: Improved error checks
* Change: Improved checking of event names
* Change: Prevent event override
* Change: Access with "this" to events of the super type
* Change: Output of Time, Date, DateTime reworked
* Change: Catching internal errors changed from ExecError to JmoError
* Change: Class JAYMO removed, static constants moved to JayMo
* Change: Object.mem now can be used also with ConstLet
* Change: Rework of Time, Date, DateTime with new bases for add/subtract
* Change: New static parser functions in JayMo-Class
* Change: API main class renamed to: JayMo
* Change: Functionality of Number.sqrt moved to .root, .sqrt allowed with no args.
* Change: File-Import improved
* Change: Functions moved from Atomic to Object: if, is, passIf, breakIf, ...
* Change: Atomic upgrade also on level ">low"
* Bugfix: Memory and speed improvement: Create DebugInfos only once.
* Bugfix: Correct toString conversion for "pipeTo" functions
* Bugfix: Using correct runtime env. for parent functions
* Bugfix: Bugfix and test for magic axis constant
* Bugfix: Using prefix typename at function/type definition
* Bugfix: All thread errors now use Lib_Output
* Bugfix: Reserved function names
* Bugfix: Difference between function and number by + and -
* Bugfix: Function cycle
* Bugfix: Error messages updated
* Bugfix: Function definition with missing brackets
* Bugfix: Throwing error if function result is invalid nil
* Bugfix: Bugfix/Clearity: Got arguments, but no argument needed
* Bugfix: Number.pow respects exact type maximum
* Bugfix: Number.cos, .tan, .sin ... and BigNumbers
* Bugfix: Set-shortcut with more than 1 argument is not allowed
* Bugfix: argsExt now checks also parent types
* Bugfix: Identify Map-Keys
* Bugfix: Empty argument
* Bugfix: Invalid typename for function parameter
* Bugfix: Variable access
* Bugfix: Bugfix and improved error message, for constants
* Bugfix: Divide Str
* Bugfix: Open argument with leading + or -
* Bugfix: Send 'it' to parent function
* Bugfix: Error messages for lonely Break/Next without a loop
* Bugfix: Bugfix for ClassFinder
* Bugfix: Access from parent to a variable at the base/root
* Bugfix: Improved checks of super type for type restrictions
* Bugfix: Parsing of arguments with brackets for extending type
* Bugfix: Init parent type constructor before current type constructor
* Bugfix: Count with 0 or negative value
* Bugfix: Str.unquote now ignore whitespaces and unquotes ' and "
* Bugfix: Missing dot before inline shell command
* Bugfix: JayMo to Java conversion now with correct error messages.
* Bugfix: Bugfix for Dir.search (NullPointerException)

=== 0.18.1 / 23.11.2021
* Bugfix: Open argument with function is not allowed
* Bugfix: Bugfix for "Sys"

=== 0.18.0 / 21.11.2021
* New:    ParserSwitches for Java imports: >class, >package, >?, >*
* New:    Atomic auto-upgrade for "DecNumber"
* New:    Optional fix length and/or value type for Map entrys
* New:    Constructor with KeyValue's for Map
* New:    Variables: Simple java types will be replaced with full java type
* New:    List.fix
* New:    Access to Java-Classes is now possible like: Java_{java.util.ArrayList}
* New:    Allow access to inner Java classes
* New:    Automatic upgrade of atomic types in open strict mode.
* New:    Format strings can now use type 't' for Type
* New:    Search files and dirs recursive with: Dir.search...
* New:    Object.pipeToCmd...
* New:    Error-Catch-All: app::@error
* New:    Autoconvert arguments for constructors and functions
* New:    Map.toList
* New:    File.readBytes, File.writeBytes
* New:    File.write( ByteArray )
* New:    Object.compare
* New:    New magic variables: infinity, +infinity, -infinity, not_a_number
* New:    Mathematical assignment for List: =++ =-- =** =//
* New:    Tree.add(KeyNode)
* New:    app.resetOutput
* New:    Cmd.wait
* New:    Open arguments allowed for function with some restrictions.
* New:    Type-Constants and Enums integrated
* New:    Enum without value, but with ordinal number and type info
* New:    Self defined Enum
* New:    Object.typeUse now also accept <Type>
* New:    Object.assertIs with variable arguments
* Change: Output of Java-Instances changed from SimpleName to FullName
* Change: Java-Object.toString improved
* Change: ParserSwitch-Shortcuts changed: >> = Import; >< removed
* Change: Allow open argument for ParserSwitch, assignment with '=' removed
* Change: File/Dir/Path: conversion to String improved
* Change: New structure for combining Date and Time to DateTime
* Change: Speed improvement for 'Str.fill'
* Change: Print, PrintErr, Describe now with newline for every argument
* Change: Error messages improved
* Change: Catch and convert all other errors/exceptions to JMo-Errors
* Change: "Sys.sleep" moved to "this._sleep"
* Change: Rework for "Cmd"
* Change: Table output now with titles
* Change: Char.toIdent improved, illegal chars will be encoded
* Change: toStrIdent -> toIdent, toStrDescribe -> toDescribe
* Change: "Object to String" improved with: toString(nested), toStringIdent
* Change: KeyValue now extends from Immutable
* Change: Rework of "Object to Str", new: ".describe", "Describe"
* Change: Output of List changed to IdentString of Items, Bugfix: No Var in List
* Change: Output of Map changed. Removed: Map.show
* Change: Output of Tree changed. Removed: Tree.show
* Change: Ident-Output of List changed to "[]"
* Change: "Atomic" Type now inherits from Type "Immutable"
* Change: Renamed: Atomic.keyValue --> .toKeyValue
* Change: Renamed: IntNumber.toBytes --> .getBytes / .toByteArray
* Change: File.write: Parameter order changed
* Bugfix: Variable atomic type upgrade
* Bugfix: Errors and messages for number overflow improved
* Bugfix: Terminate Cycle-Timer at application exit
* Bugfix: ParserSwitch '>debug' checked and optimized
* Bugfix: Combining lines
* Bugfix: for Set.put
* Bugfix: TreeBlock not closed
* Bugfix: Prevent an endless cycle, if a Sequence is within itself.
* Bugfix: Prevent senseless Count-Shortcuts like: {{{123}}}
* Bugfix: open argument and type vs. math function 'lower than'
* Bugfix: Fix for TableBlock: Cell splitting improved, control chars changed
* Bugfix: Error message for invalid hex number
* Bugfix: Tree removing nodes
* Bugfix: Let right without variable/const

=== 0.17.0 / 25.07.2021
* New:    Sys.getHostName
* New:    Sequence.select now accept 'nil' as placeholder
* New:    More functions for "ByteArray"
* New:    ByteArray improved and extended
* New:    Table.deleteRows ... to delete row(s)
* New:    A_Sequence.equals, A_Sequence.equalsLazy
* New:    3 stages for Object to String: toString, toStringExt, toStringFull
* New:    New sequence function: isFilled
* New:    New shortcut for "Set": []?
* New:    List.insert: arguments changed, New: Str.insert
* New:    Int.toBin Str.parseBin
* New:    Input.readBool
* New:    "Input" with alternate sources
* New:    Allowed again: func.execStream
* New:    New classic control object: Each
* New:    PrintErr, EchoErr, Object.printErr, Object.echoErr
* New:    New classic 'Loop' object
* New:    Single 'open argument' for types and functions
* New:    New object with one open argument
* New:    List of supporters now with comments
* New:    Line compination also with '(' at the end
* New:    Parser switch ">clearFlow" to forbid Return, Break and Next
* New:    ConstLet
* New:    Blocked Type names
* New:    String with modifier: "" Str, ""? Literal-Str, ""?? RegEx, ""¿ RegEx
* New:    All 'Immutable' with equalsLazy
* New:    New functions for 'Dir'
* New:    New shortcut notation for types: <Type>
* New:    New type "Type"
* New:    Table.rotate(left/right), .getTitles, .firstRowTitles
* New:    Map.toTable
* New:    Output-Hook for IDE
* New:    App: flag to prevent hard exit
* New:    Also accepted alias for Nil: 'null', 'NULL'
* New:    Accept numbers with exponent E
* New:    Bin,Hex,Oct now with underlines
* New:    ByteArray
* New:    Str.toBytes
* New:    Parser switch argument with '='
* New:    Atomic.format
* New:    Event-System improved, New: Cycle, app.keep
* New:    Type definition with "extends"
* New:    Alternativ function names
* New:    ShutdownHook for the App:  app::@exit
* New:    New Type: Set
* New:    New Classic-Object: Which
* New:    Date and DateTime with getDayOfWeek
* New:    Select & Update for Chars (Str, Char)
* New:    Object.assert, .assertIf, .assertIs
* New:    Integer numbers with underline, parse speed improved
* New:    Assign a object calculated to the right with: +=> -=> *=> /=> %=>
* New:    Assign calculated value to the right with +=>
* New:    Object.tryUse
* New:    Atomic.breakAtNot, .breakIfNot
* New:    New classic objects: Print, Echo
* New:    Select & Update for all sequences
* New:    List.select, List.update, Shortcut: {}
* New:    TreeBlock and lazy TreeBlock
* New:    Tableblock with ||||
* Change: Rework of event handler definition: consts allowed, vars must be spec.
* Change: Parsetime-switches ">lib" and ">import" removed.
* Change: List: Rework of mathematics
* Change: Some aliases removed from "List"
* Change: Removed: DefaultEvent
* Change: Removed: .isFilled
* Change: Group information of errors corrected
* Change: Removed: open arguments for normal functions
* Change: Forbid open argument (without brackets) for arguments
* Change: Magic constants now start with two underline chars "__"
* Change: Prevent output of "NonAtomic"
* Change: Rework of Java-Imports (Direct, Package)
* Change: Renamed: "org.jmo_lang.dev" to "org.jmo_lang.api"
* Change: New helper class: JMo4Java
* Change: Date & Time: "format" renamed to "style"
* Change: SysCmd reorganized: "" = live, ""? = pipe, ""?? = buffer
* Change: Return value of App.exec changed from toDebug to toString
* Change: "fromUnicode" renamed to "parseUnicode"
* Change: Removed from basic sequences: getSize, getLen! Ok are: getLength, len
* Change: "Switch" and case's removed in this version
* Change: Control function now can export a loop
* Change: Writing stdOut also to output file
* Change: Now a open argument is only allowed with 1 argument and no block!
* Change: Strict open argument
* Change: Very strict handling of whitespace and tab between name and args-bracket
* Change: Invalid for instantiation: True,False,Nil,Null
* Change: =~ to ~= and =~> to ~=>
* Change: No block allowed for '.mem'
* Change: "Object.do" removed, BlockExecArgs removed (.each, .pass)
* Change: "Replace Type-Alias" removed
* Change: 'app.end' removed, only valid is 'app.exit'
* Change: 'loop.end' removed, valid is only 'loop.break'
* Change: func.return is okay, func.end and func<< removed
* Change: Shortcuts for "loop" and Handle_Loop removed
* Change: HintManager removed, CLI exported, Dependencies reduced
* Change: API for external JM°-Objects reworked to use Interfaces
* Change: Redirect output
* Change: 'Range' reworked
* Change: ">clearFlow" renamed to ">cleanFlow"
* Change: Output of JMoError's with StackTrace
* Change: Either output to standard-out or to file
* Change: Next,Break,Return integrated/reworked
* Change: Don't read build number if it's not really necessary.
* Change: Access to all Java-Objects only through Prefix "Java_" or Type "Java"
* Change: Definition of Type/Function with control ability changed from {} to ()?
* Change: Definition of FuncLet with ::
* Change: loop.nr renamed to loop.lap
* Change: Format of error messages changed
* Change: Rework for handling regular expressions
* Change: Object.tryUse with 'each'
* Change: DateTimes moved to new abstract type "Immutable"
* Change: Rework for Path, File, Dir
* Change: Path-Shortcuts removed <> <[]>, tests updated
* Change: License switched to LGPL and updated to 2021
* Change: Parsing of Bin, Hex, Oct now results in type "Long"
* Change: List can now be read only
* Change: Catch errors in "hot execution" with Try
* Change: Reintegrated: _ARGS
* Change: Default implementation of toString and toDebug in EventObject
* Change: Number.format renamed to Number.style
* Change: "Namespace" transformed to "Prefix"
* Change: Timer is deprecated
* Change: Own Thread counter added, "sleep" replaced with "wait/notify"
* Change: Type definition changed to  ::MyType ^ BaseType = stdFunc
* Change: CLI improved, now with full cursor control
* Change: AutoPass removed, Strict levels balanced
* Change: Parser-Levels balanced
* Change: ".while" & ".repeat" moved from Atomic & VarLet to Object
* Change: ".handle" renamed to ".toVarLet"
* Bugfix: Terminate "Loop" on "Exit"
* Bugfix: Unclosed Get

=== 0.16.0 / 06.12.2020
* New:    New functions for Vars:  =~ , <<=, >>=, =<<, =>>, =!
* New:    Str with deep-get and more Sequence-Functions
* New:    Namespace for Java-Objects: "Java_"
* New:    Object.isNotNil
* New:    List.begin, List.append
* New:    app = , loop =
* New:    Auto-set: .title("Foo") --> .setTitle("Foo")
* New:    Object.typeUse
* New:    To catch and wrap a lot of CodeErrors: >lazyErrors
* New:    JMo-argument "-e, --exec" to directly exec a command
* New:    RandomAccessFile
* New:    Remove Dec precision and limitation: >openDec
* Change: Some MagicConst moved to MagicVar: _THIS -> this, _IT -> it, _LOOP -> loop, _FUNC -> func, _APP -> app, _JMO -> jmo, ...
* Change: ?"" to ""?
* Change: Int: and,or,xor renamed to bitAnd,bitOr,bitXor
* Change: All Try-Functions merged into one single ".try"-Function
* Change: Funclets now can also use a stream
* Change: A loop now returns the last used value
* Change: Automaticaly wrapping a Var to VarLet
* Change: ".sleep" moved from "_THIS" to "Sys"
* Change: Unified: Converting char to Dec/Float and reverse
* Change: "Next" and "Break" integrated again.
* Change: Internal Terminate-Flag to stop a running App immediately
* Change: Normalizing decimal numbers because of fractals
* Change: ?() removed
* Bugfix: Great fix for Namespaces
* Bugfix: Prevent number overflows
* Bugfix: A lot of small and greater fixes

=== 0.15.0 / 26.10.2020
* New:    Object.which
* New:    Hot-Execution
* New:    Overwriting of .toStr
* New:    New ParseTimeSwitch: >unsafeHotCode
* New:    Temporary VarLet with: Object.handle
* New:    Char.toUnicode, Str.fromUnicode
* New:    Combine also lines when '[' at the end
* New:    "Sys" to get system information
* New:    Namespaces
* New:    Str.columns, Str.table
* New:    "Tree"-Type
* New:    Parsing Tree-Block with >>>> and <<<<
* Change: Deep-Functions removed, functionality integrated in Get,Pull,Set,Put
* Change: Shortcuts for _EACH: $$, €
* Change: List changed: from position --> .start, to position --> .end
* Change: ´´ is shortcut for "Cmd.pipe", ´´? is shortcut for "Cmd.live"
* Change: Shortcut for Get & Set also with 0 arguments possible
* Change: Auto-Property improved, it's only set when a '=' follows

=== 0.14.2 / 29.09.2020
* Change: Error-Output improved
* Change: More and better tests
* Bugfix: Prevent definition of already defined Vars/Consts

=== 0.14.1 / 28.09.2020
* Change: Start-Procedure changed, Main-Class is now org.jmo-lang.JMo
* Change: Enhanced quote/unquote with Unicode chars
* Bugfix: Some fixes for CLI

=== 0.14.0 / 24.09.2020
* New:    Object.tryCatch, .tryBlock, .tryBlockCatch, the same for Try
* New:    Object.ifNil, .ifNotNil
* New:    Atomic.switch
* New:    New Functions for Str: .filter, .map, .sort, .reduce, .amount
* New:    Object.is, Object.isNot
* New:    Predefined Type for variable/const: Int i = 4
* New:    Floor-Division: /~, .floorDiv, .divFloor
* New:    Atomic: .breakAt, .breakIs, .breakIf
* New:    Auto-Pass after every function
* New:    Private functions: _private
* New:    VarArgs for Functions and Types (v...)
* New:    Else now with argument as elseif/elif: ".else(Bool b)"
* New:    Object.debug()
* New:    Sequence.deep... Get,Pull,Set,Put
* New:    Nil multiply, to get a empty List
* New:    Automatic counter for loops added: _LOOP.nr
* New:    Auto-String-Get/Set now with ':'
* New:    New functions for VarLet: .set, .while, .repeat
* New:    Object.nilUse()
* New:    JMo-Flags: -t,--test
* New:    Length-Function for all atomic types
* New:    Atomic.keyValue, Atomic -> Object = KeyValue
* New:    For-Type (C/Java-Style)
* New:    Atomic.while, Atomic.repeat
* New:    Column names for Table
* New:    VarArgs can be used with a VarLet of a List
* New:    IntNumber.for(Bool test, Object calc)
* New:    Classic command: IfNot
* New:    Error.getText
* New:    Now there are 3 Code-Styles: Classic, Stream, Slim
* New:    Many new functions for different types
* Change: All control-functions now with: _EACH, FuncLet, VarLet + _EACH
* Change: Function/Type-Definition: ":foo()%" changed to "::foo{}"
* Change: Functions: Individual initialization of parameters
* Change: §exit --> _APP.exit
* Change: MC_CONTROL removed
* Change: Changed shortcut-chars: _EACH = €, _LOOP = & or ±, _FUNC = §
* Change: Mathematic negative operator changed from '~' to '!'
* Change: Object.go renamed to .pass
* Change: ifGo --> passIf, isGo --> passIs (also for "not")
* Change: Removed: AutoBlockFunction after Function
* Change: Parser-Levels changed (level 0-4): Open, Low, Medium, High, Insane
* Change: Allow shortcut of variables
* Change: _BLOCK and _STREAM moved to _FUNC.push(x): They are only combined accessable
* Change: Standard-output of Date and Time changed to "YYYY.MM.DD HH:MM:SS"
* Change: Now accepted: nil,Nil,NIL / true, True, TRUE / false, False, FALSE
* Change: Loops: .continue renamed to .next, but .continue is a alias
* Change: Changed sequence for definitions: "::"
* Change: "For" renamed to "Count"
* Change: equally,equals,same | diffly,differs,other | ==~ == === | !=~ != !==
* Change: VarLet for shortcuts like {} changed to: .mem(:x) and => x
* Change: Now, _IT can be used like a normal variable. Also with %=++
* Change: Initialisation of Control-Objects and -Functions
* Change: Error-Output and messages improved
* Change: Strict-Levels balanced
* Bugfix: Really a lot of fixes

=== 0.13.0 / 23.05.2020
* New:    Object.proc( VarLet, Object ) / Object.tee( VarLet, Object )
* New:    List.adjust(IntNumber, Object)
* Change: Lines with leading '+' or '-' are no longer appended to the previous line.
* Change: Lazy String-Parsing (.toInt, .toDec, ...). Now nil will be returned.
* Bugfix: Some fixes for Console-Output and imports

=== 0.12.1 / 19.05.2020
* New:    Table.sort, .addRows, .combine, .addAll, .rotate, .reverse, .rows, .columns
* Change: Correct Charset for Windows-Console
* Bugfix: Bugfix for Windows-Starter

=== 0.12.0 / 16.05.2020
* New:    Functions to search first/last position in List and String
* New:    Table.get(x,y)
* New:    Number.format with tilde to ignore chars
* New:    Table(width, length, fill)
* New:    Table.getColumn
* New:    Atomic.toJava
* New:    Java-Imports, also with wildcard
* New:    Direct access to Java-Objects
* New:    Extra functions for variable assignment (=++, =--, **=, ...)
* Change: Bitwise shift,and,or,xor only for IntNumber
* Change: While.exec renamed to .each
* Change: "AutoBlock after Function" disabled by default
* Change: Range: Shortcut now uses .eachTo
* Change: Condition renamed to Use
* Change: AutoBlockFunction for Group: .get
* Change: Repeat.exec renamed to .each
* Change: Str.get and .change with negative position
* Change: CLI improved. Now it works also without stty
* Change: Str.startsWith and .endsWith now can used with more parameters
* Change: Priority of atomic types implemented and tested
* Change: Brackets get/set now can have more than one argument. Example: [3,5]
* Bugfix: Some fixes for: _THIS, Function definition, Numbers

=== 0.11.0 / 30.04.2020
* New:    New DateTimeBase for Date, Time, DateTime
* New:    Default-Function for self defined types
* New:    Functions now can process block and stream individually (_BLOCK, _STREAM)
* New:    Now you can write own Control-Objects
* New:    Sandbox for self-created Java-Objects
* New:    FunctionMap
* New:    Number: Increment/Decrement with argument
* New:    New/Changed Str-functions: from, to, fromFirst/fromLast, to..., after..., before...
* New:    New is-functions for Char (isNumber, isCaseUp, ...)
* New:    Convert of Chars: toChar = parsing, .ord / .chr = Unicode/ASC II
* New:    MagicConst-Positions (_TOP, _LEFT, _TOP_LEFT, ...)
* New:    Property-system for .get and .set (.prop --> .getProp / .prop = x --> .setProp(x))
* New:    Macro with FuncLets and arguments
* New:    FuncLet with argument
* New:    Autoblock-Function for Bool (.if)
* New:    Switch: So all case-functions moved from Object to Switch
* New:    Better handling of Java-Objects
* New:    Bool.use (clear alternative for Condition)
* Change: Logical gates: and = &&, or = ||, xor = ^^, nand = !&&, nor = !||, xnor = !^^
* Change: Map: Objects renamed to "Values"
* Change: Clear break for logical "and" / "or"
* Change: Equals (==) and differs (!=, <>) moved to Object
* Change: List.sort improved
* Change: Str and Char now inheriting from Chars
* Change: File-Handling much improved: FileSys, PathList, Path, File, Dir
* Change: Many improvements for Date, Time, DateTime, also +/- now can be used
* Change: Object.toString removed
* Change: New system for parsing a Script.
* Change: Better Exception-Handling
* Change: CmdResult replaced with FunctionMap
* Change: Result of Str.cmd is now equal to the use of Cmd
* Change: Handling of shellcommands changed and improved
* Change: Switch-Cases improved, caseIs and caseIf now returns the last Object
* Change: "Control" changed to Magic-Constant "_CONTROL"
* Change: Handling of comments improved
* Change: Multicall now updates a var
* Change: _ITS changed to _CUR
* Bugfix: Really a lot of bugs fixed!

=== 0.10.1 / 18.01.2020
* New:    UTF8-Chars in Char and Str with \uXXXX
* Change: Output-order of errors changed
* Change: Now the raw name of a variable is checked, if it is already in use
* Bugfix: A lot of fixes

=== 0.10.0 / 11.01.2020
* New:    Functional programming with FuncLet's
* New:    List.reduce, List.map, List.sort, List.filter
* New:    List.format
* New:    List.eachVertical
* New:    New MagicConst: _PI, _E
* Change: For now a Range doesn't unfold automatically in a List! Use List.unfold
* Change: Corrected: Dec.toInt
* Bugfix: Some fixes

=== 0.9.0 / 31.12.2019
* New:    New unitary concept for the processing of blocks and streams.
* New:    Better handling of System-Commands (.exec, .quiet, .forget)
* New:    Errors: For full output use .show
* New:    Auto-Block-Function for Str: .each
* New:    Auto-Block-Function for IntNumer: .timesUp
* New:    If a function clearly returns a object with Auto-Block-Function, you can use it
* New:    Code-Style-Levels balanced: open, strict, verystrict, insane
* New:    ParseError for code-errors at parse-time. CodeError is for run-time
* New:    Include other files with: >Include("file.jmo")
* New:    New Save-Mode (used for Webstart)
* New:    Direct VarLet for Shortcuts: {1,3}:a   ?{w}:b   ?(false, 'a', 'b'):c
* New:    List.swap
* Change: .do executes the auto-block-function of the current object, if it has one
* Change: .go takes the current object and executes with it the block and stream
* Bugfix: Many many fixes!

=== 0.8.4 / 02.12.2019
* New:    Every function that results clearly a object with auto-block, now can have a block
* New:    Map.each --> MapItem
* New:    Some new functions for Table
* Change: More constructors for "Time"
* Change: Functions that return a List can have an Block
* Bugfix: Different fixes

=== 0.8.3 / 24.11.2019
* New:    List.store
* New:    "For" now can be used with a "Range"
* New:    File.each
* New:    All atomic decimal types now are a parent of DecNumber
* New:    toStr for all objects
* Change: Nil is an atomic object
* Change: Better handling of incrementing variables
* Change: Improved rounding to type of variable
* Change: Improved documentation
* Change: Better block-checking
* Change: §< is now a alias to _FUNC.return(), without argument!
* Change: §= is now a alias to _FUNC.return(Object o), with argument!
* Change: Better argument-check for objects
* Bugfix: Some small bugs fixed

=== 0.8.2 / 07.11.2019
* New:    Byte,Short,Int,Long are now type of IntNumber
* New:    Nil is now atomic
* New:    Not typesafe vars now can end with ¿ or ??
* New:    _LOOP now works
* New:    Str.begin
* New:    New Objects: Repeat, Try, Error, Time, DateTime
* New:    Errors are now sorted in groups: Code, Exec, Ext, Custom
* Change: Improved documentation
* Bugfix: A lot of small bugs fixed

=== 0.8.1 / 28.10.2019
* New:    Parse-Time-Commands now starts with a leading '>'
* New:    >strictBlocks - Forbids Auto-Block and .do
* New:    >strictConsts - Forbids ° % $ $$
* New:    >strictObjects - Forbids shortcuts like () [] {} <> ?() and so on
* New:    >strictFunctions - Forbids =+-*/&|^ and so on
* New:    Improved Java-Interoperability
* New:    Better output of strings in lists
* New:    Switches: case, caseIs, caseIf
* New:    .if / .is
* New:    .go, .ifGo, .ifNotGo, .isGo, .isNotGo
* New:    "go" is now the command to execute block and stream. "do" executes the object with Auto-Block
* Change: Rewrite handling of Parse-Time-Commands.
* Bugfix: Using Var in Type-Constructor
* Bugfix: Some small bugs fixed.

=== 0.8.0 / 23.10.2019
* New:    {} now is a Shortcut to For().each
* New:    ?{} now is a Shortcut to While().do
* New:    New Types: While, If
* New:    Str.cmd
* Change: Counter renamed to For
* Change: Rewrite of Loop-System
* Change: Clearer Object-Initialization
* Change: Rewrite of handling ranges
* Bugfix: Fixed a lot of bugs

=== 0.7.5 / 16.10.2019
* New:    Str.fill to format Stings
* New:    _JMO.version
* New:    Return-Type "Same" for functions now works
* New:    List.clear
* Change: Type-unsafe Vars now ends with a ¿, nillable ends with ?
* Change: §<< removed
* Change: Documentation improved
* Bugfix: Handling empty functions
* Bugfix: Some little fixes

=== 0.7.4 / 07.10.2019
* New:    List.insert
* New:    QuickSort works!
* Bugfix: Major bugfix at handling instances

=== 0.7.3 / 02.10.2019
* New:    Values from ".each" and ".times" can passed directly to an variable: Example: 3.times(:a)
* Change: Group: Now it makes a difference whether you call it with "()" or with "Group"
* Change: Condition: Now it makes a difference whether you call it with "?()" or with "Condition"
* Change: Assign a value to a variable with "mem", now needs a VarLet. Example: 5.mem(:a); a.print
* Bugfix: Some fixes for the CLI

=== 0.7.2 / 26.09.2019
* New:    New List-Function "concat" with alias ++
* Change: Complete Code-Review to make it OpenSource

=== 0.7.1
* New:    Exit the Application with: _APP.exit or §exit
* New:    List.filter with _EACH or $$; Example: [3,9,7,2].filter($$ < 5).print; [3,9,7,2].filter(_EACH < 5).print
* New:    Cool List-Functions for cutting: left, right, cut, area, from, to
* New:    New Atomic Types: Byte, Short, Long, Float
* Change: Major update at storing and handling variables.
* Bugfix: Important fixes for the Function-Multi-Call-System
* Bugfix: Different fixes and improvements

=== 0.6.38
* New:    Condition works with:  Condition(Bool cond, Object then, Object else) ... or with: ?(Bool cond, Object then, Object else)
* New:    Variables ending with ?? now are Type-unsafe and nillable
* Change: Raw Objects, without Auto-Block-Function couldn't have Blocks anymore.
